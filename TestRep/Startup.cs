﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestRep.Startup))]
namespace TestRep
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
